describe("The Game", function() {

  var game;

  beforeEach(function(){
    game = new Game();
  });

  it("exists", function(){
    expect(game).toEqual(jasmine.any(Game));
  });

  it("contains a grid with 5 rows and 5 columns, all 0s", function(){
    testgrid = game.grid(5,5); // create 5x5 grid
    expect(testgrid.length).toEqual(5); // check height
    for(var h=0;h<testgrid.length;h++){ // of every row:
      expect(testgrid[h].length).toEqual(5); // check width
      for(var w=0;w<testgrid[h].length;w++){ // of every cell:
        expect(testgrid[h][w]).toEqual(0); // Check value is 0. TODO: Change to 0 *or* 1
      }
    }
  });

});